package com.argo.clustering;

import com.argo.tree.centroid.MutableCentroidNode;
import com.argo.tree.centroid.MutableCentroidNodeBuilder;
import com.argo.tree.centroid.VectorMeanCentroidCalculator;

public class Main {

	private static MutableCentroidNodeBuilder<Double[]> builder = 
			new MutableCentroidNodeBuilder<Double[]>(new VectorMeanCentroidCalculator());
	
	public static void main(String[] args) {
		Double[] LEAF1_VALUE = { 5D, 4D, 3D };
		Double[] LEAF2_VALUE = { 8D, 4D, 7D };
		Double[] LEAF3_VALUE = { 0D, 5D, 8D };
		
		MutableCentroidNode<Double[]> root = builder.createNode("root");
		MutableCentroidNode<Double[]> node1 = builder.createNode("node1");
		MutableCentroidNode<Double[]> leaf1 = builder.createNode("leaf1", LEAF1_VALUE);
		MutableCentroidNode<Double[]> leaf2 = builder.createNode("leaf2", LEAF2_VALUE);
		MutableCentroidNode<Double[]> leaf3 = builder.createNode("leaf3", LEAF3_VALUE);
		
		root.addChild(node1);
		root.addChild(leaf1);
		node1.addChild(leaf2);
		node1.addChild(leaf3);
		
		ClusterAnalyzer analyzer = new ClusterAnalyzer();
		analyzer.analyzeCluster(root);
	}
	
}
