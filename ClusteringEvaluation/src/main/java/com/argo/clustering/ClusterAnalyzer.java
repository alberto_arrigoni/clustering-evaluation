package com.argo.clustering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import cc.mallet.util.Maths;

import com.argo.tree.Node;

public class ClusterAnalyzer {

	public void analyzeCluster(Node<Double[]> root) {
		Collection<? extends Node<Double[]>> childrenList = root.children();
		for (Node<Double[]> node : childrenList) {
			analyzeCluster(node);
		}
		analyzeSubtree(root);
	}
	
	private void analyzeSubtree(Node<Double[]> subtree) {
		List<Node<Double[]>> grandsonList = extractGrandchildren(subtree);
		if (grandsonList.size() == 0) {
			return;
		}
		Node<Double[]> referenceNode = grandsonList.get(0);
		for (int i = 1; i < grandsonList.size(); i++) {
			Node<Double[]> comparisonNode = grandsonList.get(i);
			Double distance = computeDistance(referenceNode, comparisonNode);
			System.out.println(referenceNode.getName() + ", " + comparisonNode.getName() + ": " + distance);
		}
	}
	
	private List<Node<Double[]>> extractGrandchildren(Node<Double[]> root) {
		if (root.isLeaf()) {
			return Collections.<Node<Double[]>>emptyList();
		}
		List<Node<Double[]>> grandchildrenList = new ArrayList<Node<Double[]>>();
		for (Node<Double[]> child : root.children()) {
			grandchildrenList.addAll(child.children());
		}
		return grandchildrenList;
	}
	
	private Double computeDistance(Node<Double[]> node1, Node<Double[]> node2) {
		return Maths.jensenShannonDivergence(
				unboxArray(node1.getValue()),
				unboxArray(node2.getValue()));
	}
	
	private double[] unboxArray(Double[] boxedArray) {
		double[] unboxedArray = new double[boxedArray.length];
		for (int i = 0; i < boxedArray.length; i++) {
			unboxedArray[i] = boxedArray[i];
		}
		return unboxedArray;
	}
	
}
