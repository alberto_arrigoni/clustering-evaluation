package com.argo.clustering;

import java.util.HashMap;
import java.util.Map;

import com.argo.tree.Node;

public class Distance {

	private Node<Double[]> referenceNode;
	
	private Map<Node<Double[]>, Double> distanceMap;
	
	protected Distance(Node<Double[]> referenceNode) {
		this.referenceNode = referenceNode;
		distanceMap = new HashMap<Node<Double[]>, Double>();
	}
	
	protected void addDistance(Node<Double[]> node, Double distance) {
		distanceMap.put(node, distance);
	}
	
}
