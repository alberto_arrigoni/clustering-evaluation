package com.argo.tree;

import java.util.Collection;

public interface Node<E> {

	public String getName();

	public E getValue();
	
	public Node<E> parent();

	public Collection<? extends Node<E>> children();
	
	public int countChildren();

	public boolean isLeaf();
	
}
