package com.argo.tree;

public interface NodeBuilder<E, G extends Node<E>> {

	public G createNode(String name);
	
	public G createNode(String name, E value);
	
}
