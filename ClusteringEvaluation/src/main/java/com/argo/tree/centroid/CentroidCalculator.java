package com.argo.tree.centroid;

import java.util.List;

import com.argo.tree.Node;

public interface CentroidCalculator<E> {

	public E calculateCentroidValue(List<? extends Node<E>> nodeList);
	
}
