package com.argo.tree.centroid;

import java.util.Arrays;
import java.util.List;

import com.argo.tree.Node;

public class VectorMedianCentroidCalculator implements CentroidCalculator<Double[]> {

	@Override
	public Double[] calculateCentroidValue(List<? extends Node<Double[]>> nodeList) {
		int valueLength = nodeList.get(0).getValue().length;
		Double[] centroidValue = new Double[valueLength];
		for (int i = 0; i < valueLength; i++) {
			centroidValue[i] = median(extractRow(i, nodeList));
		}
		return centroidValue;
	}
	
	private Double[] extractRow(int rowNumber, List<? extends Node<Double[]>> nodeList) {
		Double[] doubleArray = new Double[nodeList.size()];
		for (int i = 0; i < nodeList.size(); i++) {
			doubleArray[i] = nodeList.get(i).getValue()[rowNumber];
		}
		return doubleArray;
	}
	
	private Double median(Double[] valueArray) {
		Arrays.sort(valueArray);
		int middlePosition = valueArray.length/2;
		if (valueArray.length % 2 == 0) {
			return (valueArray[middlePosition - 1] + valueArray[middlePosition])/2;
		} else {
			return valueArray[middlePosition];
		}
	}
	
}
