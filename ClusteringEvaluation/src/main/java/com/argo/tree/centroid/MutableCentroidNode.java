package com.argo.tree.centroid;

import java.util.ArrayList;
import java.util.List;

import com.argo.tree.Node;

public class MutableCentroidNode<E> implements Node<E> {

	private String name;
	
	private E value;
	
	private MutableCentroidNode<E> parent;
	
	private List<MutableCentroidNode<E>> childrenList;
	
	private CentroidCalculator<E> centroidCalculator;
	
	protected MutableCentroidNode(String name, CentroidCalculator<E> centroidCalculator) {
		this.name = name;
		this.centroidCalculator = centroidCalculator;
		this.value = null;
		this.parent = null;
		this.childrenList = new ArrayList<MutableCentroidNode<E>>();
	}
	
	protected MutableCentroidNode(String name, CentroidCalculator<E> centroidCalculator, E value) {
		this(name, centroidCalculator);
		this.value = value;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public E getValue() {
		if (isLeaf()) {
			return value;
		} else {
			return centroidValue();
		}
	}
	
	private E centroidValue() {
		if (value == null) {
			value = centroidCalculator.calculateCentroidValue(childrenList);
		}
		return value;
	}
	
	@Override
	public MutableCentroidNode<E> parent() {
		return parent;
	}

	@Override
	public List<MutableCentroidNode<E>> children() {
		return childrenList;
	}

	@Override
	public int countChildren() {
		return childrenList.size();
	}

	@Override
	public boolean isLeaf() {
		return childrenList.size() == 0;
	}
	
	public void addChild(MutableCentroidNode<E> child) {
		childrenList.add(child);
		child.setParent(this);
		invalidateValue();
	}
	
	private void invalidateValue() {
		value = null;
		if (parent != null) {
			parent.invalidateValue();
		}
	}
	
	private void setParent(MutableCentroidNode<E> parent) {
		this.parent = parent;
	}

	public String structure() {
		return structure("");
	}
	
	private String structure(String prefix) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(name + " (" + getValue() + ")\n");
		for (int i = 0; i < childrenList.size(); i++) {
			buffer.append(prefix + "|\n");
			buffer.append(prefix + "+---");
			MutableCentroidNode<E> node = childrenList.get(i);
			String childPrefix = (i == childrenList.size() - 1) ? "    " : "|   ";
			buffer.append(node.structure(prefix + childPrefix));
		}
		return buffer.toString();
	}

}
