package com.argo.tree.centroid;

import java.util.List;

import com.argo.tree.Node;

public class VectorMeanCentroidCalculator implements CentroidCalculator<Double[]> {

	@Override
	public Double[] calculateCentroidValue(List<? extends Node<Double[]>> nodeList) {
		Double[] centroidValue = nodeList.get(0).getValue();
		for (int i = 1; i < nodeList.size(); i++) {
			centroidValue = arraySum(centroidValue, nodeList.get(i).getValue());
		}
		for (int i = 0; i < centroidValue.length; i++) {
			centroidValue[i] = centroidValue[i] / nodeList.size();
		}
		return centroidValue;
	}
	
	private Double[] arraySum(Double[] arrayA, Double[] arrayB) {
		if (arrayA.length != arrayB.length) {
			throw new IllegalArgumentException("Cannot add arrays with different lenghts");
		}
		Double[] result = new Double[arrayA.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = arrayA[i] + arrayB[i];
		}
		return result;
	}

}
