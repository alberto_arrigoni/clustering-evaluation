package com.argo.tree.centroid;

import java.util.List;

import com.argo.tree.Node;

public class ScalarMeanCentroidCalculator implements CentroidCalculator<Integer>{

	@Override
	public Integer calculateCentroidValue(List<? extends Node<Integer>> nodeList) {
		Integer sum = 0;
		for (Node<Integer> node : nodeList) {
			sum += node.getValue();
		}
		return sum / nodeList.size();
	}
	
}
