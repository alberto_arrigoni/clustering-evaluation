package com.argo.tree.centroid;

import com.argo.tree.NodeBuilder;

public class MutableCentroidNodeBuilder<E> 
		implements NodeBuilder<E, MutableCentroidNode<E>>{

	private CentroidCalculator<E> centroidCalculator;
	
	public MutableCentroidNodeBuilder(CentroidCalculator<E> centroidCalculator) {
		this.centroidCalculator = centroidCalculator;
	}
	
	@Override
	public MutableCentroidNode<E> createNode(String name) {
		return new MutableCentroidNode<E>(name, centroidCalculator);
	}
	
	@Override
	public MutableCentroidNode<E> createNode(String name, E value) {
		return new MutableCentroidNode<E>(name, centroidCalculator, value);	
	}
	
}
