package com.argo.tree;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.argo.tree.centroid.MutableCentroidNode;
import com.argo.tree.centroid.MutableCentroidNodeBuilder;
import com.argo.tree.centroid.ScalarMeanCentroidCalculator;

public class ScalarTreeTest {

	MutableCentroidNodeBuilder<Integer> builder = 
		new MutableCentroidNodeBuilder<Integer>(new ScalarMeanCentroidCalculator());
	
	static final String ROOT_NAME = "root";
	static final String NODE1_NAME = "node1";
	static final String LEAF1_NAME = "leaf1";
	static final String LEAF2_NAME = "leaf2";
	static final String LEAF3_NAME = "leaf3";
	
	static final Integer LEAF1_VALUE = 1;
	static final Integer LEAF2_VALUE = 2;
	static final Integer LEAF3_VALUE = 3;
	
	MutableCentroidNode<Integer> root = builder.createNode(ROOT_NAME);
	MutableCentroidNode<Integer> node1 = builder.createNode(NODE1_NAME);
	MutableCentroidNode<Integer> leaf1 = builder.createNode(LEAF1_NAME, LEAF1_VALUE);
	MutableCentroidNode<Integer> leaf2 = builder.createNode(LEAF2_NAME, LEAF2_VALUE);
	MutableCentroidNode<Integer> leaf3 = builder.createNode(LEAF3_NAME, LEAF3_VALUE);
	
	@Before
	public void setup() {
		root.addChild(node1);
		root.addChild(leaf1);
		node1.addChild(leaf2);
		node1.addChild(leaf3);
	}
	
	@Test
	public void leafValue() {
		assertEquals(LEAF1_VALUE, leaf1.getValue());
		assertEquals(LEAF2_VALUE, leaf2.getValue());
		assertEquals(LEAF3_VALUE, leaf3.getValue());
	}
	
	@Test
	public void scalarMeanCentroidValue() {
		Integer expectedNode1Value = (leaf2.getValue() + leaf3.getValue()) / 2;
		assertEquals(expectedNode1Value, node1.getValue());
		
		Integer expectedRootValue = (expectedNode1Value + leaf1.getValue()) / 2;
		assertEquals(expectedRootValue, root.getValue());
	}
	
}
