package com.argo.tree;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.argo.tree.centroid.MutableCentroidNode;
import com.argo.tree.centroid.MutableCentroidNodeBuilder;
import com.argo.tree.centroid.VectorMeanCentroidCalculator;

public class VectorTreeTest {

	MutableCentroidNodeBuilder<Double[]> builder = 
			new MutableCentroidNodeBuilder<Double[]>(new VectorMeanCentroidCalculator());
			
	static final String ROOT_NAME = "root";
	static final String NODE1_NAME = "node1";
	static final String LEAF1_NAME = "leaf1";
	static final String LEAF2_NAME = "leaf2";
	static final String LEAF3_NAME = "leaf3";
			
	static final Double[] LEAF1_VALUE = { 5D, 4D, 3D };
	static final Double[] LEAF2_VALUE = { 8D, 4D, 7D };
	static final Double[] LEAF3_VALUE = { 0D, 5D, 8D };
	
	MutableCentroidNode<Double[]> root = builder.createNode(ROOT_NAME);
	MutableCentroidNode<Double[]> node1 = builder.createNode(NODE1_NAME);
	MutableCentroidNode<Double[]> leaf1 = builder.createNode(LEAF1_NAME, LEAF1_VALUE);
	MutableCentroidNode<Double[]> leaf2 = builder.createNode(LEAF2_NAME, LEAF2_VALUE);
	MutableCentroidNode<Double[]> leaf3 = builder.createNode(LEAF3_NAME, LEAF3_VALUE);
	
	@Before
	public void setup() {
		root.addChild(node1);
		root.addChild(leaf1);
		node1.addChild(leaf2);
		node1.addChild(leaf3);
	}
	
	@Test
	public void leafValue() {
		assertArrayEquals(LEAF1_VALUE, leaf1.getValue());
		assertArrayEquals(LEAF2_VALUE, leaf2.getValue());
		assertArrayEquals(LEAF3_VALUE, leaf3.getValue());
	}
	
}
