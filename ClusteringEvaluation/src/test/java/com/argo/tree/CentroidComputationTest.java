package com.argo.tree;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import com.argo.tree.centroid.MutableCentroidNode;
import com.argo.tree.centroid.MutableCentroidNodeBuilder;
import com.argo.tree.centroid.VectorMeanCentroidCalculator;
import com.argo.tree.centroid.VectorMedianCentroidCalculator;

public class CentroidComputationTest {
	
	static final String ROOT_NAME = "root";
	static final String NODE1_NAME = "node1";
	static final String LEAF1_NAME = "leaf1";
	static final String LEAF2_NAME = "leaf2";
	static final String LEAF3_NAME = "leaf3";
	static final String LEAF4_NAME = "leaf4";
	static final String LEAF5_NAME = "leaf5";
	static final String LEAF6_NAME = "leaf6";
			
	static final Double[] LEAF1_VALUE = { 5D, 4D, 3D };
	static final Double[] LEAF2_VALUE = { 8D, 4D, 7D };
	static final Double[] LEAF3_VALUE = { 0D, 5D, 8D };
	static final Double[] LEAF4_VALUE = { 0D, 8D, 8D };
	static final Double[] LEAF5_VALUE = { 12D, 7D, 5D };
	static final Double[] LEAF6_VALUE = { 2D, 3D, 7D };
	
	
	@Test
	public void vectorMeanCentroidValue() {
		MutableCentroidNodeBuilder<Double[]> builder = 
				new MutableCentroidNodeBuilder<Double[]>(new VectorMeanCentroidCalculator());
		
		MutableCentroidNode<Double[]> root = builder.createNode(ROOT_NAME);
		MutableCentroidNode<Double[]> node1 = builder.createNode(NODE1_NAME);
		MutableCentroidNode<Double[]> leaf1 = builder.createNode(LEAF1_NAME, LEAF1_VALUE);
		MutableCentroidNode<Double[]> leaf2 = builder.createNode(LEAF2_NAME, LEAF2_VALUE);
		MutableCentroidNode<Double[]> leaf3 = builder.createNode(LEAF3_NAME, LEAF3_VALUE);
		
		root.addChild(node1);
		root.addChild(leaf1);
		node1.addChild(leaf2);
		node1.addChild(leaf3);
		
		Double[] node1Mean = { 
				(LEAF2_VALUE[0] + LEAF3_VALUE[0]) / 2, 
				(LEAF2_VALUE[1] + LEAF3_VALUE[1]) / 2,
				(LEAF2_VALUE[2] + LEAF3_VALUE[2]) / 2
				};
		assertArrayEquals(node1Mean, node1.getValue());
		
		Double[] rootMean = {
				(node1Mean[0] + LEAF1_VALUE[0]) / 2,
				(node1Mean[1] + LEAF1_VALUE[1]) / 2,
				(node1Mean[2] + LEAF1_VALUE[2]) / 2
		};
		assertArrayEquals(rootMean, root.getValue());
	}
	
	@Test
	public void vectorMedianCentroidValue1() {
		MutableCentroidNodeBuilder<Double[]> builder = 
				new MutableCentroidNodeBuilder<Double[]>(new VectorMedianCentroidCalculator());
		
		MutableCentroidNode<Double[]> root = builder.createNode(ROOT_NAME);
		MutableCentroidNode<Double[]> node1 = builder.createNode(NODE1_NAME);
		MutableCentroidNode<Double[]> leaf1 = builder.createNode(LEAF1_NAME, LEAF1_VALUE);
		MutableCentroidNode<Double[]> leaf2 = builder.createNode(LEAF2_NAME, LEAF2_VALUE);
		MutableCentroidNode<Double[]> leaf3 = builder.createNode(LEAF3_NAME, LEAF3_VALUE);
		MutableCentroidNode<Double[]> leaf4 = builder.createNode(LEAF4_NAME, LEAF4_VALUE);
		
		root.addChild(node1);
		root.addChild(leaf1);
		node1.addChild(leaf2);
		node1.addChild(leaf3);
		node1.addChild(leaf4);
		
		Double[] node1Median = { 
				LEAF3_VALUE[0], 
				LEAF3_VALUE[1],
				LEAF4_VALUE[2]
				};
		assertArrayEquals(node1Median, node1.getValue());
		
		Double[] rootMedian = {
				(node1Median[0] + LEAF1_VALUE[0]) / 2,
				(node1Median[1] + LEAF1_VALUE[1]) / 2,
				(node1Median[2] + LEAF1_VALUE[2]) / 2
		};
		assertArrayEquals(rootMedian, root.getValue());
	}
	
	@Test
	public void vectorMedianCentroidValue2() {
		MutableCentroidNodeBuilder<Double[]> builder = 
				new MutableCentroidNodeBuilder<Double[]>(new VectorMedianCentroidCalculator());
		
		MutableCentroidNode<Double[]> root = builder.createNode(ROOT_NAME);
		MutableCentroidNode<Double[]> node1 = builder.createNode(NODE1_NAME);
		MutableCentroidNode<Double[]> leaf1 = builder.createNode(LEAF1_NAME, LEAF1_VALUE);
		MutableCentroidNode<Double[]> leaf2 = builder.createNode(LEAF2_NAME, LEAF2_VALUE);
		MutableCentroidNode<Double[]> leaf3 = builder.createNode(LEAF3_NAME, LEAF3_VALUE);
		MutableCentroidNode<Double[]> leaf4 = builder.createNode(LEAF4_NAME, LEAF4_VALUE);
		MutableCentroidNode<Double[]> leaf5 = builder.createNode(LEAF5_NAME, LEAF5_VALUE);
		MutableCentroidNode<Double[]> leaf6 = builder.createNode(LEAF6_NAME, LEAF6_VALUE);
		
		root.addChild(node1);
		root.addChild(leaf1);
		root.addChild(leaf5);
		root.addChild(leaf6);
		node1.addChild(leaf2);
		node1.addChild(leaf3);
		node1.addChild(leaf4);
		
		Double[] node1Median = { 0D, 5D, 8D };
		assertArrayEquals(node1Median, node1.getValue());
		
		Double[] rootMedian = {
				(2D + 5D) / 2,
				(4D + 5D) / 2,
				(5D + 7D) / 2
		};
		assertArrayEquals(rootMedian, root.getValue());
	}
	
}
